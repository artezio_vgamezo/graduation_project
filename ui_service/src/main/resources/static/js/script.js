$(function () {
    var $submit     = $('#submit');
    var $source     = $('#source');
    var $executeBtn = $('#execute');
    var $result     = $('#result');
    var $resultBox  = $('#resultBox');
    var $input      = $('#input');
    var $inputBox   = $('#inputBox');
    $input.addClass('col-12');

    if ($source.val().length === 0) {
        $submit.attr('disabled','disabled');
        $executeBtn.attr('disabled','disabled');
    }

    $source.on('input', function (e) {
        var value = e.target.value;
        if (value === "") {
            $submit.attr('disabled','disabled');
            $executeBtn.attr('disabled','disabled');
        } else {
            $submit.removeAttr('disabled');
            $executeBtn.removeAttr('disabled');
        }
    });

    $executeBtn.on('click', function () {
        $input.removeClass('col-12').addClass('col-6');
        $result.show();
        var source = $source.val();
        var input = $inputBox.val();
        var isError = false;
        var msg = '';
        $.post({
            url: 'http://127.0.0.1:1234/execute/',
            data: {
                'source': source,
                'input': input
            },
            crossDomain: true,

            success: function (data) {
                isError = data.error;
                msg = data.message;
                $resultBox.val('\n----------------------------------------\n' + $resultBox.val());
                $resultBox.val(msg + $resultBox.val());
                $resultBox.val(
                    (isError ? 'Произошла ошибка : \n' : 'Результат : \n')
                    + $resultBox.val()
                )
            },
        });
    });
});