package com.graduation.ui_service.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * @author vgamezo
 */
@Component
public class UiService {

    @Value("${main.server.path}")
    private String SERVER_PATH;

    public String getSource(String id) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(this.SERVER_PATH + "get/" + id, String.class);
    }
}
