package com.graduation.ui_service.controllers;

import com.graduation.ui_service.service.UiService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * @author vgamezo
 */
@Controller
public class UIController {

    private UiService uiService;

    public UIController(UiService uiService) {
        this.uiService = uiService;
    }

    @GetMapping("")
    public String index() {
        return "index";
    }

    @GetMapping("/features")
    public String features() {
        return "features";
    }

    @GetMapping("/error")
    public String error() {
        return "error";
    }

    @GetMapping("/pricing")
    public String pricing() {
        return "pricing";
    }

    @GetMapping("/{id}")
    public String get(@PathVariable String id, Model model) {
        String source = uiService.getSource(id);
        if (source == null) {
            model.addAttribute("error", "Code not found or not exists.");
        }
        model.addAttribute("source", source);
        return "index";
    }
}
