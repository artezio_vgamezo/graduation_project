package com.graduation.storage_service.util;

import java.util.Random;

public class KeyGenerator {
    private static final long OFFSET = 0b110100100101110100100101L;
    private static final String ALPHABET = "_qwertyuioplkjhgfdsazxcvbnm0192837465QMWNEBRVTCYXUZIAOSPDLFKGJH-";//26 + 10 + 26

    private long token;
    private static KeyGenerator ourInstance = new KeyGenerator();

    public static KeyGenerator getInstance() {
        return ourInstance;
    }

    private KeyGenerator() {
        token = new Random().nextLong();
    }

    public String next() {
        token += OFFSET;
        return format(token);
    }

    private String format(long token) {
        return ""
                + ALPHABET.charAt((int) ((token & 0x000000003F000000L) >>> 24))
                + ALPHABET.charAt((int) ((token & 0x0000000000FC0000L) >>> 18))
                + ALPHABET.charAt((int) ((token & 0x000000000003F000L) >>> 12))
                + ALPHABET.charAt((int) ((token & 0x0000000000000FC0L) >>> 6))
                + ALPHABET.charAt((int) ((token & 0x000000000000003FL)));
    }
}