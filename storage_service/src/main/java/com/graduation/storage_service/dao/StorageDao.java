package com.graduation.storage_service.dao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

@Component
public class StorageDao {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public StorageDao(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public String get(String id) {
        try {
            return namedParameterJdbcTemplate.queryForObject(
                    "SELECT `content` FROM codes WHERE `id` = :id;",
                    Collections.singletonMap("id", id),
                    (rs, rowNum) -> rs.getString("content")
            );
        } catch (DataAccessException ex) {
            return "";
        }
    }

    public boolean put(String id, String content) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("id", id);
        map.put("content", content);
        int rowsAffected;
        try {
            rowsAffected = namedParameterJdbcTemplate.update("INSERT INTO `codes` VALUES (:id, :content)", map);
        } catch (DataAccessException ex) {
            rowsAffected = 0;
        }
        return rowsAffected == 1;
    }
}