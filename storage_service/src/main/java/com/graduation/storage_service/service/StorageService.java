package com.graduation.storage_service.service;

import com.graduation.storage_service.dao.StorageDao;
import org.springframework.stereotype.Service;

@Service
public class StorageService {

    private StorageDao storageDao;

    public StorageService(StorageDao storageDao) {
        this.storageDao = storageDao;
    }

    public boolean put(String key, String source) {
        boolean isSuccess = storageDao.put(key, source);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            return isSuccess;
        }
    }

    public String get(String id) {
        return storageDao.get(id);
    }
}
