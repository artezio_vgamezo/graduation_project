package com.graduation.storage_service.rest;

import com.graduation.storage_service.service.StorageService;
import com.graduation.storage_service.util.KeyGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author vgamezo
 */
@RestController
public class StorageRestController {

    private final StorageService storageService;

    @Value("${main.server.path}")
    private String SERVER_PATH;

    private final KeyGenerator keyGenerator;

    public StorageRestController(StorageService storageService) {
        this.storageService = storageService;
        this.keyGenerator = KeyGenerator.getInstance();
    }

    @PostMapping("/save")
    public ModelAndView save(@RequestParam String source) {
        String key;
        boolean isAdded;

        do {
            key = keyGenerator.next();
            isAdded = this.storageService.put(key, source);
        } while (!isAdded);

        return new ModelAndView("redirect:" + SERVER_PATH + key);
    }

    @GetMapping("/get/{id}")
    public String get(@PathVariable String id) {
        return storageService.get(id);
    }
}
