package com.graduating.execute_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExecuteServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExecuteServiceApplication.class, args);
    }

}
