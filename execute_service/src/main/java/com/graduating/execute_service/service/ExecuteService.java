package com.graduating.execute_service.service;

import com.graduating.execute_service.dto.Result;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.util.Scanner;

@Service
public class ExecuteService {

    /**
     * Компилирует и, в случае успеха, запускает код.
     *
     * Происходит то же самое, что и при попытке запустить код, записанный в Main.java, используя консоль:
     * <pre>
     *     <code>javac Main.java </code> //Создался файл Main.class
     *     <code>java Main </code> // Запускается код на выполнение.
     * </pre>
     * При этом файл находится в пакете по умолчанию, что
     * @param source исходный код.
     * @param input входные данные.
     * @return объект класса {@link Result} Результат выполнения.
     */
    public Result execute(String source, String input) {

        File inputFile = new File("input");
        File outputFile = new File("output");
        File errorFile = new File("error");
        File mainFileJava = new File("Main.java");

        if (!this.updateFiles(inputFile, outputFile, errorFile, mainFileJava)) {
            System.out.println("Updating file is failed.");
            return Result.UNEXPECTED_ERROR;
        }

        if (!this.print(source, mainFileJava)) {
            return Result.UNEXPECTED_ERROR;
        }

        if (!this.print(input, inputFile)) {
            return Result.UNEXPECTED_ERROR;
        }

        if (!this.execute("javac.exe", "Main.java", inputFile, outputFile, errorFile)) {
            return Result.UNEXPECTED_ERROR;
        }

        try {
            // Без этого не работает. Не удалять!!!
            // Надо дать системе время для создания файликов и прочих важных дел.
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return Result.UNEXPECTED_ERROR;
        }
        String errors = getResult(errorFile);

        if (errors.length() != 0) {
            return new Result(true, "Compilation error.\n" + errors);
        }

        FileInputStream inputStream;
        try {
            inputStream = new FileInputStream(inputFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return Result.UNEXPECTED_ERROR;
        }
        System.setIn(inputStream);
        this.execute("java.exe", "Main", inputFile, outputFile, errorFile);

        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            return Result.UNEXPECTED_ERROR;
        }

        try {
            // Без этого не работает. Не удалять!!!
            // Надо дать системе время для создания файликов и прочих важных дел.
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return Result.UNEXPECTED_ERROR;
        }

        errors = getResult(errorFile);

        if (errors.length() != 0) {
            return new Result(true, errors);
        }

        return new Result(false, getResult(outputFile));
    }

    private String getResult(File file) {
        Scanner in;
        try {
            in = new Scanner(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";
        }
        StringBuilder result = new StringBuilder();
        while (in.hasNext()) {
            result.append(in.nextLine());
            result.append("\n");
        }
        in.close();
        return result.toString();
    }

    /**
     * Записывает текст <code>source</code> в файл <code>to</code>.
     * @param source исходный код.
     * @param to файл назначения.
     * @return <code>true</code>, если всё прошло без ошибок,
     *         <code>false</code>, если что-то при выполнении произошли ошибки.
     */
    private boolean print(String source, File to) {
        try {
            Writer out = new FileWriter(to);
            out.write(source);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Обновляет файлы путем их удаления и создания.
     * @param files набор файлов.
     * @return <code>true</code>, если все файлы обновились,
     *         <code>false</code>, если некоторые файлы остались необновленными.
     */
    private boolean updateFiles(File... files) {
        for (File file : files) {
            if (file.exists()) {
                try {
                    Files.delete(file.toPath());
                    System.out.println(file + " was deleted");
                } catch (IOException e) {
                    System.out.println(file + " wasn't deleted");
                    return false;
                }
            }
            try {
                Files.createFile(file.toPath());
                System.out.println(file + " was created");
            } catch (IOException e) {
                System.out.println(file + " wasn't created");
                return false;
            }
        }
        return true;
    }

    /**
     * Выполняет команду <code>command</code> с параметрами <code>args</code>.
     * При этом:
     * 1. Консольный ввод данных будет происходить из файла <code>inputFile</code>;
     * 2. Консольный вывод данных будет перенаправлен в файл <code>outputFile</code>;
     * 3. Вывод ошибок будет осуществлен в <code>errorFile</code>.
     *
     * @param command команда
     * @param args аргументы команды
     * @param inputFile входные данные
     * @param outputFile выходные данные
     * @param errorFile сообщения об ошибках
     * @return <code>true</code>, если выполнение команды произошло без ошибок,
     *         <code>false</code>, если при выполнении произошли ошибки и программа аварийно завершилась.
     */
    private boolean execute(String command, String args, File inputFile, File outputFile, File errorFile) {
        ProcessBuilder processBuilder = new ProcessBuilder(command, args);
        processBuilder.redirectInput(inputFile);
        processBuilder.redirectOutput(outputFile);
        processBuilder.redirectError(errorFile);

        try {
            processBuilder.start();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
