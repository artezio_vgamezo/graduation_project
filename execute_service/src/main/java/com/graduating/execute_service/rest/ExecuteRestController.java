package com.graduating.execute_service.rest;

import com.graduating.execute_service.dto.Result;
import com.graduating.execute_service.service.ExecuteService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class ExecuteRestController {

    private ExecuteService executeService;

    public ExecuteRestController(ExecuteService executeService) {
        this.executeService = executeService;
    }

    @PostMapping("/execute")
    public Result execute(
            @RequestParam String source,
            @RequestParam String input,
            HttpServletRequest request,
            HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        return executeService.execute(source, input);
    }
}
