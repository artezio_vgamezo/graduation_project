package com.graduating.execute_service.dto;

public class Result {

    public static Result UNEXPECTED_ERROR = new Result(
            true,
            "Unexpected error has occurred. Repeat request later."
    );

    private boolean error;
    private String message;

    public Result() {}

    public Result(boolean error, String message) {
        this.error = error;
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
